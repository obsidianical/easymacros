use clap::Parser;
use easymacros::x11_safe_wrapper::{Keysym, string_to_keysym, XDisplay};
use std::ffi::CString;
use std::process::{Command, exit};
use std::time::Duration;
use std::{fs, thread};
use std::io::stdin;
use x11::keysym::XK_Shift_L;
use easymacros::chartbl::CHARTBL;

/// Macro player module for easymacros. It's compatible with xmacro macros.
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
	/// The file that contains the macro to run.
	#[clap(value_parser, value_name = "input_file", value_hint = clap::ValueHint::FilePath)]
	input_file: Option<std::path::PathBuf>,
	/// Display to run the macro on. This uses the $DISPLAY environment variable by default.
	#[clap(short = 'D', long)]
	display: Option<String>,
	/// Delay for events to be sent.
	#[clap(short, long)]
	delay: Option<u64>,
}

fn main() {
	let args = Args::parse();

	let display = get_remote(args.display);
	let delay = args.delay.unwrap_or(10);

	if let Some(input_file_path) = args.input_file {
		let input_file_contents = fs::read_to_string(input_file_path).expect("Couldn't read macro file");

		for instruction in input_file_contents.lines() {
			run_instruction(instruction, &display, delay);
		}
	} else {
		println!("No input file specified, reading from stdin.");
		let stdin = stdin();

		loop {
			let mut line = String::new();
			stdin.read_line(&mut line).expect("Couldn't read line from stdin");
			// Without this it crashes because apparently it doesn't properly read the next input line?
			println!();
			line = line.trim().to_string();
			run_instruction(&*line, &display, delay);
		}
	}

	display.close();
}

fn get_remote(display_name: Option<String>) -> XDisplay {
	let display = XDisplay::open(display_name);

	if !display.has_xtest() {
		eprintln!("XTest not supported!");
		display.close();
		exit(1)
	}

	display.grab_control();
	display.sync();

	display
}

fn run_instruction(instruction: &str, dpy: &XDisplay, delay: u64) {
	let instruction_split: Vec<&str> = instruction.split(' ').collect();

	match instruction_split[0] {
		"Delay" => thread::sleep(Duration::from_millis(instruction_split[1].parse().unwrap())),
		"ButtonPress" => dpy.send_fake_buttonpress(instruction_split[1].parse().unwrap(), delay),
		"ButtonRelease" => dpy.send_fake_buttonrelease(instruction_split[1].parse().unwrap(), delay),
		"MotionNotify" => dpy.send_fake_motion_event(instruction_split[1].parse().unwrap(), instruction_split[2].parse().unwrap(), delay),
		"KeyCodePress" => dpy.send_fake_keypress_from_code(instruction_split[1].parse().unwrap(), delay),
		"KeyCodeRelease" => dpy.send_fake_keyrelease_from_code(instruction_split[1].parse().unwrap(), delay),
		"KeySymPress" => dpy.send_fake_keypress_from_keysym(instruction_split[1].parse().unwrap(), delay),
		"KeySymRelease" => dpy.send_fake_keyrelease_from_keysym(instruction_split[1].parse().unwrap(), delay),

		"KeySym" => {
			let key: Keysym = instruction_split[1].parse().unwrap();
			dpy.send_fake_keypress_from_keysym(key, delay);
			dpy.send_fake_keyrelease_from_keysym(key, delay);
		}
		"KeyStrPress" => dpy.send_fake_keypress_from_string(CString::new(instruction_split[1]).unwrap().as_bytes(), delay),
		"KeyStrRelease" => dpy.send_fake_keyrelease_from_string(CString::new(instruction_split[1]).unwrap().as_bytes(), delay),
		"KeyStr" => {
			let keystring = CString::new(instruction_split[1]).unwrap();
			dpy.send_fake_keypress_from_string(keystring.as_bytes(), delay);
			dpy.send_fake_keyrelease_from_string(keystring.as_bytes(), delay);
		}
		"String" => {
			for c in instruction["String".len() + 1..].chars() {
				send_char(dpy, c, delay);
			}
		}
		"ExecBlock" | "ExecNoBlock" => {
			let mut command = Command::new(instruction_split[1]);
			for arg in &instruction_split[2..] {
				command.arg(arg);
			}
			if instruction_split[0] == "ExecBlock" {
				command.status().unwrap();
			} else {
				command.spawn().unwrap();
			}
		}
		c => panic!("Unknown command {:?}", instruction_split),
	}
}

fn send_char(dpy: &XDisplay, c: char, delay: u64) {
	// get keystring from character and turn it into a keysym
	let keysym = string_to_keysym(CHARTBL[c as usize].as_ref());
	let keycode = dpy.keysym_to_keycode(keysym);

	if keycode == 0 {
		eprintln!("No keycode found for character '{}'", c);
		return;
	}

	let map_ks = dpy.get_keyboard_mapping(keycode, 1);
	if map_ks[0] == 0 {
		eprintln!("XGetKeyboardMapping failed (keycode: {})", keycode);
		return;
	}

	let (ks_lower, ks_upper) = dpy.convert_case(keysym);

	// check if shift has to be pressed as well
	let mut shift_needed = true;
	if keysym == map_ks[0] && (keysym == ks_lower && keysym == ks_upper) {
		shift_needed = false;
	}
	if keysym == ks_lower && keysym != ks_upper {
		shift_needed = false;
	}

	if shift_needed { dpy.send_fake_keypress_from_keysym(XK_Shift_L as Keysym, delay); }
	dpy.send_fake_keypress_from_code(keycode, delay);
	dpy.send_fake_keyrelease_from_code(keycode, delay);
	if shift_needed { dpy.send_fake_keyrelease_from_keysym(XK_Shift_L as Keysym, delay); }
}