extern crate core;

use std::ffi::c_void;
use std::os::raw::{c_char};
use std::process::{exit};
use std::ptr::addr_of;

use clap::Parser;
use x11::xlib::{CurrentTime, GrabModeAsync, GrabModeSync, GrabSuccess, KeyPressMask, SyncPointer, Time, XFree, XKeyEvent};
use x11::xrecord::{XRecordAllocRange, XRecordEndOfData, XRecordFreeData, XRecordInterceptData, XRecordStartOfData};

use easymacros::{BUTTONPRESS_U8, BUTTONRELEASE_U8, Instructions, KEYPRESS_U8, KEYRELEASE_U8, MOTIONNOTIFY_U8, Position};
use easymacros::ev_callback_data::EvCallbackData;
use easymacros::macro_writer::MacroWriter;
use easymacros::x11_safe_wrapper::{Keycode, XDisplay};

/// Macro recording module for easymacros. Outputs are partially compatible with xmacro.
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
	/// The file to record the macro to. Defaults to writing to stdout.
	#[clap(value_parser, value_name = "output_file", value_hint = clap::ValueHint::FilePath)]
	output_file: Option<std::path::PathBuf>,
	/// Display to run the macro on. This uses the $DISPLAY environment variable by default.
	#[clap(short = 'D', long)]
	display: Option<String>,
	/// Max Delay in milliseconds for macro delays
	#[clap(short, long)]
	max_delay: Option<u64>,
	/// Allow delay capturing in recording output. If this flag is set, the program will ignore the max_delay.
	#[clap(short, long)]
	ignore_delay_capturing: bool,
}

fn main() {
	let args = Args::parse();

	let display = XDisplay::open(args.display.clone());
	let recorded_display = XDisplay::open(args.display.clone());
	let stop_key = get_stop_key(display);
	let writer = MacroWriter::new(args.output_file, args.ignore_delay_capturing);

	event_loop(
		display,
		recorded_display,
		stop_key,
		writer,
		args.max_delay,
	);

	display.close();
}

fn get_stop_key(display: XDisplay) -> Keycode {
	let screen = display.get_default_screen();

	let root = display.get_root_window(screen);
	let potential_err = display.grab_keyboard(root, false, GrabModeSync, GrabModeAsync, CurrentTime);

	if potential_err != GrabSuccess {
		eprintln!("Couldn't grab keyboard!");
		exit(1);
	}

	println!("Press the key you want to use to stop recording the macro.");

	let stop_key;
	loop {
		display.allow_events(SyncPointer, CurrentTime);
		let ev = XKeyEvent::from(display.window_event(root, KeyPressMask));
		stop_key = ev.keycode;
		break;
	}

	display.ungrab_keyboard(CurrentTime);
	display.ungrab_pointer(CurrentTime);

	stop_key
}

fn event_loop(
	xdpy: XDisplay,
	recdpy: XDisplay,
	stop_key: Keycode,
	mut writer: MacroWriter,
	max_delay: Option<Time>,
) {
	let protocol_ranges = unsafe { XRecordAllocRange() };

	let pointer_pos: Position<i16> = Position::from(xdpy.query_pointer_pos());

	if pointer_pos != Position(-1, -1) {
		writer.write(Instructions::MotionNotify(pointer_pos))
	}

	let ctx = recdpy.create_record_context(protocol_ranges);
	let ev_cb_data = EvCallbackData::new(writer, xdpy, recdpy, ctx, stop_key, pointer_pos, max_delay);

	if !recdpy.enable_context_async(ctx, Some(ev_callback), addr_of!(ev_cb_data) as *mut c_char) {
		panic!("Failed to enable record context")
	}
	while ev_cb_data.working {
		recdpy.process_replies();
	}

	xdpy.disable_context(ctx);
	xdpy.free_context(ctx);
	unsafe { XFree(protocol_ranges as *mut c_void) };
}

unsafe extern "C" fn ev_callback(closure: *mut c_char, intercept_data: *mut XRecordInterceptData) {
	let data = &mut *(closure as *mut EvCallbackData);
	let intercept_data = &mut *intercept_data;

	if intercept_data.category == XRecordStartOfData {
		println!("Got start of data!");
		data.last_event = intercept_data.server_time;
		XRecordFreeData(intercept_data);
		return;
	} else if intercept_data.category == XRecordEndOfData {
		println!("Got end of data!");
		XRecordFreeData(intercept_data);
		return;
	}

	let ev_type = *(intercept_data.data as *const u8);

	if data.pos.0 == 0 || data.pos.1 == -1 {
		if ev_type == MOTIONNOTIFY_U8 {
			data.update_pos(intercept_data);
			data.write_pos();
		} else {
			println!("Move your cursor so the macro can start with a fixed cursor position!
			Skipping event...");
		}
	} else if data.no_keypress_yet && ev_type == KEYRELEASE_U8 {
		println!("Skipping KeyRelease without recorded KeyPress...");
	} else {
		match ev_type {
			MOTIONNOTIFY_U8 => {
				data.update_pos(intercept_data);

				if !data.moving {
					data.moving = true;
				}
			}
			KEYPRESS_U8 | KEYRELEASE_U8 => {
				let kc: u8 = *((intercept_data.data as usize + 1) as *const u8);
				let keyname = data.xdpy.keycode_to_string(kc as u32);

				if ev_type == KEYPRESS_U8 && kc == data.stop_key as u8 {
					data.working = false;
				} else {
					if ev_type == KEYPRESS_U8 {
						data.no_keypress_yet = false;
					}

					data.maybe_write_delay(intercept_data.server_time);

					if data.ptr_is_moving() { data.write_pos(); }

					data.writer.write(if ev_type == KEYPRESS_U8 {
						Instructions::KeyStrPress(keyname)
					} else {
						Instructions::KeyStrRelease(keyname)
					});
				}
			}
			BUTTONPRESS_U8 | BUTTONRELEASE_U8 => {
				let bc: u8 = *((intercept_data.data as usize + 1) as *const u8);

				data.maybe_write_delay(intercept_data.server_time);

				if data.ptr_is_moving() { data.write_pos(); }

				data.writer.write(if ev_type == BUTTONPRESS_U8 {
					Instructions::ButtonPress(bc)
				} else {
					Instructions::ButtonRelease(bc)
				});
			}
			_ => eprintln!("Unknown event type: {:?}", ev_type)
		}
	}

	data.ev_nr += 2;
	XRecordFreeData(intercept_data)
}

