extern crate core;

use std::ffi::CString;
use std::fmt::{Display, Formatter};

use x11::xlib::{ButtonPress, ButtonRelease, KeyPress, KeyRelease, MotionNotify, Time};

use crate::x11_safe_wrapper::{Keycode, Keysym};

pub mod x11_safe_wrapper;
pub mod chartbl;
pub mod macro_writer;
pub mod ev_callback_data;

pub const KEYPRESS_U8: u8 = KeyPress as u8;
pub const KEYRELEASE_U8: u8 = KeyRelease as u8;
pub const BUTTONPRESS_U8: u8 = ButtonPress as u8;
pub const BUTTONRELEASE_U8: u8 = ButtonRelease as u8;
pub const MOTIONNOTIFY_U8: u8 = MotionNotify as u8;

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Position<T> (pub T, pub T);

impl From<Position<i32>> for Position<i16> {
    fn from(pos: Position<i32>) -> Self {
        Self (pos.0 as i16, pos.1 as i16)
    }
}

impl From<(i32, i32)> for Position<i32> {
    fn from(v: (i32, i32)) -> Self {
        Position(v.0, v.1)
    }
}

impl From<(i32, i32)> for Position<i16> {
    fn from(v: (i32, i32)) -> Position<i16> {
        let p1: Position<i32> = Position::from(v);
        Position::from(p1)
    }
}

pub enum Instructions<'a> {
    Delay(Time),
    ButtonPress(u8),
    ButtonRelease(u8),
    MotionNotify(Position<i16>),
    KeyCodePress(Keycode),
    KeyCodeRelease(Keycode),
    KeySymPress(Keysym),
    KeySymRelease(Keysym),
    KeySym(Keysym),
    KeyStrPress(CString),
    KeyStrRelease(CString),
    KeyStr(CString),
    String(&'a str),
    ExecBlock(&'a str),
    ExecNoBlock(&'a str),
}

impl Display for Instructions<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}",
            match self {
                Instructions::Delay(d) => format!("Delay {}", d),
                Instructions::ButtonPress(b) => format!("ButtonPress {}", b),
                Instructions::ButtonRelease(b) => format!("ButtonRelease {}", b),
                Instructions::MotionNotify(pos) => format!("MotionNotify {} {}", pos.0, pos.1),
                Instructions::KeyCodePress(kc) => format!("KeyCodePress {}", kc),
                Instructions::KeyCodeRelease(kc) => format!("KeyCodeRelease {}", kc),
                Instructions::KeySymPress(ks) => format!("KeySymPress {}", ks),
                Instructions::KeySymRelease(ks) => format!("KeySymRelease {}", ks),
                Instructions::KeySym(ks) => format!("KeySym {}", ks),
                Instructions::KeyStrPress(kstr) => format!("KeyStrPress {}", kstr.to_str().unwrap()),
                Instructions::KeyStrRelease(kstr) => format!("KeyStrRelease {}", kstr.to_str().unwrap()),
                Instructions::KeyStr(kstr) => format!("KeyStr {}", kstr.to_str().unwrap()),
                Instructions::String(str) => format!("String {}", str),
                Instructions::ExecBlock(cmd) => format!("ExecBlock {}", cmd),
                Instructions::ExecNoBlock(cmd) => format!("ExecNoBlock {}", cmd),
            },
        )
    }
}
