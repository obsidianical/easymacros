use std::fs::File;
use std::io;
use std::io::Write;
use crate::Instructions;

pub struct MacroWriter {
	outfile: Box<dyn Write>,
	ignore_delay_capturing: bool,
}

impl MacroWriter {
	pub fn new(outfile: Option<std::path::PathBuf>, ignore_delay_capturing: bool) -> Self {
		Self {
			outfile: if let Some(outfile) = outfile {
				Box::new(File::create(outfile).expect("Failed to create output file"))
			} else {
				Box::new(io::stdout())
			},
			ignore_delay_capturing,
		}
	}

	pub fn write(&mut self, instruction: Instructions) {
		if self.ignore_delay_capturing {
			if let Instructions::Delay(_) = instruction {
				return;
			}
		}

		writeln!(&mut self.outfile, "{}", instruction).expect("Failed to write instruction to outfile");
	}
}

