{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = { self, nixpkgs, flake-utils, naersk }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages."${system}";
        naersk-lib = naersk.lib."${system}";
      in
        rec {
          # `nix build`
          packages.easymacros = naersk-lib.buildPackage {
            pname = "easymacros";
            root = ./.;
            nativeBuildInputs = with pkgs; [ pkg-config xorg.libX11 xorg.libXtst ];
          };
          defaultPackage = packages.easymacros;

          # `nix run`
          apps.easymacros = flake-utils.lib.mkApp {
            drv = packages.easymacros;
          };
          defaultApp = apps.easymacros;

          # `nix develop`
          devShell = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [ rustc cargo ];
          };
        }
    );
}
