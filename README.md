# easymacros

This program is inspired by [**xmacro**](https://github.com/Ortega-Dan/xmacroIncludingDelayCapturing/), however it isn't `xmacro`.

**Note**: This program is for personal use; I will modify it as I deem necessary, but feel free to open issues for suggestions or bug reports. Contributions are welcome! 

#

<details>
<summary><h2>:pen_ballpoint: TODOs :notepad_spiral:</h2></summary>

- [x] Playing macros (xmacro like)
  - [x] Delay support
  - [x] KeySym/KeyCode/KeyStr action support
  - [x] MotionNotify and button support
  - [x] String typing support (Not too high priority, but I'll add it some time probably)
  - [x] ExecBlock/ExecNoBlock support (not high priority)
    - [x] ExecBlock
    - [x] ExecNoBlock
- [x] Recording macros (xmacro like)
  - [x] Delay
  - [x] Keyboard actions
  - [x] Mouse actions
- [x] Utilities for playing macros
  - [ ] Ignoring delays when playing
  - [x] Event delay support
- [ ] Rebrand?
	- [ ] new name
	- [ ] logo
- [ ] macro language (easymacros daemon?)
  - [ ] basic interpreter/compiler to fast intermediate lang
  - [ ] stdlib
    - [ ] xlib stuff (get window handles/ids)
	- [ ] easy xtst/xrecord etc abstractions
	- [ ] number/text inputs
	- [ ] clipboard
	- [ ] basic gui features
	- [ ] filesystem stuff
	- [ ] shell stuff
	- [ ] find image/track image/wait for image...
  - [ ] event listeners

#

</details>

<details>
<summary><h2>❔ Ideas 🧠</h2></summary>

I may or may not do these, but they sound fun to implement to me!

- [ ] additional macro features
  - [ ] relative cursor movements
  - [ ] screenshotting whole/parts ot the screen (using external programs)
- [ ] Macro daemon kind of thing to listen in the background for keyboard shortcuts?
  - [ ] the daemon itself
  - [ ] config file
  - [ ] rofi integration
  - [ ] dmenu integration
  - [ ] custom macro manager
- [ ] macro language?
  - [ ] Sending keys
  - [ ] mouse movements/events
  - [ ] control flow stuff/math/just normal scripting language stuff
  - [ ] reading/writing to clipboard
  - [ ] calling/recording other macros
  - [ ] running commands
- [ ] GUI Macro editor which is actually user friendly
#
</details>

<details>
<summary><h2>:desktop: Platform support :compression:</h2></summary>

- [x] Linux x11 (*only tested on `i3wm`*)
- [ ] Linux Wayland (*makes heavy use of X apis, I will only do this if I myself switch to Wayland. I'm open to suggestions how to do it though!*)
- [ ] MacOS (*Might work because of [XQuartz](https://www.xquartz.org/)?*)
- [ ] Windows (*Yeah, I'm not doing that myself. Unless I have to use Windows for anything.*) <!--(gentoolinux-girl): We both know you are not using Windows. xD-->
#
</details>

<details>
<summary><h2>:inbox_tray: Installation :newspaper:</h2></summary>

Currently only manually possible via `cargo build --release` and then moving the result into your `$PATH`.
#
</details>
